# Django site URLs, for 'mysite' project.  Django v1.5
# SEE http://docs.djangoproject.com/en/dev/topics/http/urls/

from django.conf.urls import patterns, url, include

from django.views.generic import TemplateView
#from django.views.generic.simple import direct_to_template

from django.views.generic import DetailView, ListView
from polls.models import Poll

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

#  url( regex, view, [kwargs], [name] )

urlpatterns = patterns('',
    url(r'^polls/', include('polls.urls')),

    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    
    url(r'^contact/thanks/', 'simplecontact.views.thanks'),
    url(r'^contact/', 'simplecontact.views.contact'),
    
#   url(r'^basic/$', direct_to_template, {'template': 'todos/basic.html'}),
    url(r'^basic/$', TemplateView.as_view(template_name='todos/basic.html')),

    
#   url(r'^api/', include('mysite.api.urls', 'mysite.piston_testapp.urls')),
    url(r'^grappelli/', include('grappelli.urls')),
    )

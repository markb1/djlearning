# "Polls" URLs, Django v1.5
# SEE http://docs.djangoproject.com/en/dev/topics/http/urls/

from django.conf.urls import patterns, url, include
from django.views.generic import ListView, DetailView
from polls.models import Poll
from polls import views

# Poll "index" page:  displays the latest few polls.
# Poll "detail" page:  displays a poll question, with no results but with a Form for voting.
# Poll "results" page:  displays results for a particular poll.
# Vote action:  handles voting for a particular choice in a particular poll.

#  Url( regex, view, [kwargs], [name] )

urlpatterns = patterns('',

    # ex: /polls/
    url (r'^$',
         ListView.as_view(queryset=Poll.objects.order_by('-pub_date')[:5],
                          context_object_name='latest_poll_list',
                          template_name='polls/index.html'),
                          name='polls.index'),

    # these Generic Views expect a primary key called 'pk' (vs. 'poll_id')
    # ex: /polls/5/
    url (r'^(?P<pk>\d+)/$',
         DetailView.as_view(model=Poll,
                            template_name='polls/detail.html'),
                            name='polls.pk'),

    # ex: /polls/5/results/
    url(r'^(?P<pk>\d+)/results/$',
        DetailView.as_view(model=Poll,
                           template_name='polls/results.html'),
                           name='polls_results'),

    # ex: /polls/5/vote/
    url(r'^(?P<poll_id>\d+)/vote/$',
        'polls.views.vote',
        name='polls.vote'),
)

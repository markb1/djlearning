from django.db import models
from django.utils import timezone
import datetime

# A Poll has a question and a publication-date.
# A Choice has two fields: the text of the choice and a vote tally.
#   Each Choice is associated with a Poll.

class Poll(models.Model):
    question = models.CharField(max_length=200)
    # use optional 1st positional arg to a Field, for human-readable name
    pub_date = models.DateTimeField('Publication date')

    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=7)

    was_published_recently.short_description = 'Published this week?'
    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True

    def was_published_today(self):
        return self.pub_date.date() == datetime.date.today()

    was_published_today.short_description = 'Published today?'
    was_published_today.boolean = True

    def __unicode__(self):
        return self.question


class Choice(models.Model):
    poll = models.ForeignKey(Poll)  ## each Choice is assoc. with a Poll
    choice = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __unicode__(self):
        return self.choice

# manage.py sql polls  ## dump CREATE TABLE sql stmts
# manage.py validate  ## Check for any errors in models
# manage.py sqlcustom polls  ## Outputs any custom SQL statements (such as table modifications or constraints) that are defined for the application.
# manage.py sqlindexes polls  ## Outputs the CREATE INDEX statements for models
# manage.py sqlall polls  ## reports all SQL from sql, sqlcustom, and sqlindexes commands
#
# manage.py sqlclear polls  ## DANGER! do DROP TABLE statements for this app, according to which tables already exist in your database (if any).

from django.contrib import admin
from polls.models import Poll, Choice

class ChoiceInline(admin.TabularInline): #admin.StackedInline
    model = Choice
    extra = 3  ## provide 3 Choice fields

class PollAdmin(admin.ModelAdmin):
    list_display = ('question', 'pub_date',
                    'was_published_today', 'was_published_recently')  ## columns
    list_filter = ['pub_date']  ## filter Polls based on...

    fieldsets = [
        (None,               {'fields': ['question']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    inlines = [ChoiceInline]

    search_fields = ['question']  ## search on Polls by...
    date_hierarchy = 'pub_date'   ## drill down Polls by...


# now the Poll can be modified!
admin.site.register(Poll, PollAdmin)


# Create your views here.
from django.http import HttpResponseRedirect  # HttpResponse
from django.template import Context, RequestContext
from django.shortcuts import render_to_response, get_object_or_404
from django.core.urlresolvers import reverse

from polls.models import Poll, Choice

def index(request):
    #    output = ', '.join([p.question for p in latest_poll_list])
    #    return HttpResponse(output)

    latest_poll_list = Poll.objects.all().order_by('-pub_date')[:5]
    return  render_to_response('polls/index.html',
                               Context({'latest_poll_list': latest_poll_list})
                                )

def detail(request, poll_id):
    """ """
    p = get_object_or_404(Poll, pk=poll_id)
    return render_to_response('polls/detail.html', {'poll': p},
                               context_instance=RequestContext(request))
    # try:
    #     p = Poll.objects.get(pk=poll_id)
    # except Poll.DoesNotExist:
    #     raise Http404

    return render_to_response('polls/detail.html', {'poll': p})
#    return HttpResponse("You're looking at poll %s." % poll_id)


def results(request, poll_id):
    """ """
    p = get_object_or_404(Poll, pk=poll_id)
    return render_to_response('polls/results.html', {'poll': p})
    #return HttpResponse("You're looking at the results of poll %s." % poll_id)


def vote(request, poll_id):
    """ """
    p = get_object_or_404(Poll, pk=poll_id)
    try:
        selected_choice = p.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # then redisplay the poll voting form
        return render_to_response('polls/detail.html', {
            'poll': p,
            'error_message': "You did not select a choice.",
            }, context_instance=RequestContext(request))
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Return an HttpResponseRedirect after successfully handling
        # POST data - to prevent data from being posted twice when a
        # user hits the Back button
        return HttpResponseRedirect(reverse('polls_results', args=(p.id,)))
        #return HttpResponseRedirect(reverse('polls.views.results', args=(p.id,)))

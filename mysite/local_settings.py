import os

# http://www.robgolding.com/blog/2010/05/03/extending-settings-variables-with-local_settings-py-in-django/#comment-284944491
class Settings(object):
    def __init__(self):
        import settings
        self.settings = settings
    def __getattr__(self, name):
        return getattr(self.settings, name)

settings = Settings()

INSTALLED_APPS = settings.INSTALLED_APPS + (
    'polls',
    'simplecontact',
    'todos',
#    'django.contrib.flatpages',
#    'tinymce',
)

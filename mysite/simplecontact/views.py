# Create your views here.

#from mysite.simple-contact-form import models
import models

from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext

from django.core.mail import send_mail

def contact(request):
    """ your comments here!
    """
    if request.method == 'POST':                # If form has been submitted...
        form = models.ContactForm(request.POST) # A form bound to the POST data

        if form.is_valid(): # All validation rules pass
            # Process the data in form.cleaned_data
            subject = form.cleaned_data['subject']
            message = form.cleaned_data['message']
            sender = form.cleaned_data['sender']
            cc_myself = form.cleaned_data['cc_myself']

            recipients = ['mbiggers@localhost']

            if cc_myself:
                recipients.append(sender)

            send_mail(subject, message, sender, recipients)
            # Redirect after POST
            return HttpResponseRedirect('/contact/thanks/')

    else:
        form = models.ContactForm() # An unbound form

    return render_to_response('contacts/contacts.html', {'theform': form,},
                              context_instance=RequestContext(request))


def thanks(request):
    """ comments
    """
    return render_to_response('contacts/thanks.html')
